export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;

    while (firstFighterHealth > 0 && secondFighterHealth > 0) {
        secondFighterHealth = secondFighterHealth - getDamage(firstFighter, secondFighter);
        firstFighterHealth = firstFighterHealth - getDamage(secondFighter, firstFighter);
    }
    return firstFighterHealth > secondFighterHealth ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
    let hitPower = getHitPower(attacker);
    let blockPower = getBlockPower(enemy);
    let damage = blockPower > hitPower ? 0 : hitPower - blockPower;
    return damage;
}

export function getHitPower(fighter) {
    return (Math.random() + 1) * fighter.attack;
}

export function getBlockPower(fighter) {
    return (Math.random() + 1) * fighter.defense;
}
