import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export function showWinnerModal(fighter) {
    const title = 'Winner';
    const bodyElement = createWinnerView(fighter);
    showModal({title, bodyElement});
}

function createWinnerView(fighter) {
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});

    const fighterImage = createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes: {'src': fighter.source}
    });

    const nameElement = createElement({tagName: 'span', className: 'fighter-name'});
    nameElement.innerText = fighter.name;

    fighterDetails.append(nameElement);
    fighterDetails.append(fighterImage);

    return fighterDetails;
}
