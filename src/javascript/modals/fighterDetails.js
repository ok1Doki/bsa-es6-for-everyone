import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});

    const fighterImage = createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes: {'src': fighter.source}
    });

    const div = createElement({tagName: 'div', className: 'nes-table-responsive'});
    const table = createElement({tagName: 'table', className: 'nes-table'});
    const tbody = createElement({tagName: 'tbody'});

    tbody.append(createRow('Name', fighter.name));
    tbody.append(createRow('Health', fighter.health));
    tbody.append(createRow('Attack', fighter.attack));
    tbody.append(createRow('Defense', fighter.defense));
    table.append(tbody);
    div.append(table);

    fighterDetails.append(fighterImage);
    fighterDetails.append(div);

    return fighterDetails;
}

function createRow(label, value) {
    const tr = createElement({tagName: 'tr'});
    const td1 = createElement({tagName: 'td'});
    const td2 = createElement({tagName: 'td'});
    td1.innerText = label;
    td2.innerText = value;
    tr.append(td1);
    tr.append(td2)
    return tr;
}
